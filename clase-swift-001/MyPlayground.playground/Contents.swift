//: Playground - noun: a place where people can play

// var nombre = "Carlos rivera"

/*
var nombre2 = "Carlos rivera"
var nombre3 = "Carlos rivera"
var nombre4 = "Carlos rivera"
*/

var nombre5: String = "Carlos rivera"

print(nombre5)
dump(nombre5)

var contador: Int = 0

for _ in 1..<10 {
    contador += 1
    print(contador)
}


//Tipos de Datos

// String
// Int
// Double
// Float
// Bool


var variable = "Gabriel"
let constante = 20

//constante = 30 // Da Error

var notas = "Diez"
let numero = 10

type(of: notas)
type(of: numero)















