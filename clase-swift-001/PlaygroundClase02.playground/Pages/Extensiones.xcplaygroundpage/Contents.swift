//: [Previous](@previous)

import Foundation

var valor1: Int = 6

extension Int {
    
    func inv() -> Int {
        return -self
    }
}


valor1.inv()
45.inv()


struct Complex {
    let x: Double
    let y: Double
}

let a = Complex(x: 3, y: -4)


// Extension del Complex

extension Complex {
    var magnitude: Double {
        
        return (( x * x ) + ( y * y )).squareRoot()
    }
}

let complex = Complex(x: 2, y: 4)
complex.magnitude






