//: Playground - noun: a place where people can play

import UIKit

typealias Entero = Int
typealias Author = String

var numero: Entero = 5

type(of: numero)

let author: Author = "George"

let _ = "Cualquier dato"

func suma1 (nombreExternoA nombreInternoA: Int, nombreExternoB nombreInternoB: Int) -> Int {
    return nombreInternoA + nombreInternoB
}

func suma2 (_ nombreInternoA: Int, _ nombreInternoB: Int) -> Int {
    return nombreInternoA + nombreInternoB
}

func suma3 (A: Int, B: Int) -> Int {
    return A + B
}


var respuesta1 = suma1(nombreExternoA: 6, nombreExternoB: 7)
var respuesta2 = suma2(5, 7)
var respuesta3 = suma3(A: 7, B: 9)
















