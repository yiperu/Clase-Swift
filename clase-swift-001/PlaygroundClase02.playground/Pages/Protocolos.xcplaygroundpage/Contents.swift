//: [Previous](@previous)

import Foundation

struct Complex {
    let x: Double
    let y: Double
}

let a = Complex(x: 3, y: -4)


// Extension del Complex

extension Complex {
    var magnitude: Double {
        
        return (( x * x ) + ( y * y )).squareRoot()
    }
}

let complex = Complex(x: 2, y: 4)
complex.magnitude



var valor1: Int = 6

extension Complex: CustomStringConvertible {
    
    var description: String {
        get {
            return "\(x) + \(y)j"
        }
    }
}

complex.description


protocol Answerable {
    var answer: Int { get }
}

// Implementacion por defecto
extension Answerable {
    var answer: Int {
        return 42
    }
}

//extension String: Answerable, protoco2, protoco3 { // Tambien se puede hacer
extension String: Answerable {
    
}

// Es asi como se deberian implementar los protocolos
//extension String: protoco2 {
//
//}
//
//extension String: protoco3 {
//
//}

let pregunta: String = "Die que es la vida?"
pregunta.answer















