//: [Previous](@previous)

import Foundation

let tupla1 = (2,3)
tupla1.0
tupla1.1

var httpResponse = ("Ok", 200)
type(of: httpResponse)

//httpResponse = (300, "Error")  // Error
//
//httpResponse = (300, "Error", 4)   // Error

func datosDiv(a: Int, b: Int) -> (cociente: Int,resto: Int) {
    
    return ( (a / b) , (a % b) )
}

var resultado = datosDiv(a: 15, b: 3)
resultado.cociente // resultado.0
resultado.resto

// = = = = =

typealias HTTPStatus = (code: Int, text: String)
typealias HTTPRequest = String
typealias HTTPResponse = (mbody: String, status: HTTPStatus)

func httpServer(request: HTTPRequest) -> HTTPResponse {
    return ("Works", (200, "OK") )
}

var respuesta1 = httpServer(request: "index.html")
respuesta1.mbody
respuesta1.status

var (_, dato2) = httpServer(request: "index.html")

dato2














