//: [Previous](@previous)

import Foundation

struct Complex {
    let x: Double
    let y: Double
}


class LibroAnonimo {
    let titulo: String
    
    init(title: String) {
        titulo = title
    }
}

class Book: LibroAnonimo {
    
    let autores: [String]
    
    init(title: String, autores: [String]) {
        self.autores = autores
        super.init(title: title)
    }
}

extension Book {
    
    convenience init(title: String, autor: [String]) {
        self.init(title: title, autores: autor)
    }
}



final class Personaje {
    
    var nombre: String
    var apellido:String
    
    
    init(nombre: String, apellido: String) {
        self.nombre = nombre
        self.apellido = apellido
    }
    
    var completo: String {
        get {
            return nombre + " " + apellido
        }
    }
}

let carlos: Personaje = Personaje(nombre: "Juan", apellido: "Chavez")
carlos.nombre  // getter
carlos.nombre = "Carlos" // setter
carlos.completo















// Ejemplo de paso por copia de Int (q es un Struct)
var x: Int = 23
var y = x

 x = 5
 y // Aun vale 23



