//: [Previous](@previous)

import Foundation

struct Complex {
    let x: Double
    let y: Double
}

let a = Complex(x: 3, y: -4)

// En Swift los constructores se crea con  init

struct Math {
    
    static func pi() -> Double {
        return 3.1415
    }
}

Math.pi()

struct Point {
    var x: Double
    var y: Double
    
    mutating func moveTo(x: Double, y: Double)  {
        self.x = x
        self.y = y
    }
}

var punto = Point(x: 12, y: 7)
punto.x
punto.y

punto.moveTo(x: 0, y: 0)
punto.x
punto.y


// Extension del Complex

extension Complex {
    var magnitude: Double {
        
        return (( x * x ) + ( y * y )).squareRoot()
    }
}

let complex = Complex(x: 2, y: 4)
complex.magnitude








