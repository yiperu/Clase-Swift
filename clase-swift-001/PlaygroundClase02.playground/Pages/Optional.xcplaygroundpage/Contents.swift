//: [Previous](@previous)

import Foundation

// Optional

var cualquiera: String?

type(of: cualquiera)

//cualquiera = "Dato Valido"
cualquiera = nil

// FORMA SEGURA DE MANIPULAR UN OPTIONAL
// Con Ambito interno

if let valido = cualquiera {
    print(valido)
}

// Con Ambito Externo
guard let valido2 = cualquiera else {
    throw NSError(domain: "Optional vacia", code: 0, userInfo: nil)
}

print(valido2)







